﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//In general, this script manages what panels and fields are visible and when.
public class SceneController : MonoBehaviour
{
    [SerializeField]
    GameObject guidePanel = null;

    [SerializeField]
    GameObject labelPanel = null;

    [SerializeField]
    GameObject spherePointer = null;

    [SerializeField]
    GameObject messagePanel = null;

    private bool guideOpen = false;
    private bool toggledOff = false;
    private bool editToggledOff = false;

    [HideInInspector]
    public bool clickable = false;

    private GameObject scripts = null;
    private MoveObj moveObj = null;

    private ClickManager clickManager = null;
    private DataBase dataBase = null;

    private string currentBubbleName = null;


    // Start is called before the first frame update
    void Start()
    {
        Screen.SetResolution(Screen.width + 2000, Screen.height + 2000, false, 0);

        guidePanel.SetActive(false);
        guideOpen = false;
        clickable = false;
        
        Screen.fullScreen = true;

        scripts = GameObject.Find("Scripts");
        moveObj = scripts.GetComponent<MoveObj>();

        clickManager = GameObject.Find("ClickManager").GetComponent<ClickManager>();
        dataBase = GameObject.Find("DataBase").GetComponent<DataBase>();
    }
    //open/close the help guide
    public void ToggleGuide()
    {
        if(guideOpen == false)
        {
            guidePanel.SetActive(true);
            guideOpen = true;
        }
        else
        {
            guidePanel.SetActive(false);
            guideOpen = false;
        }
    }
    //hide/show labels from view
    public void ToggleLabelView()
    {
        GameObject m_object = GameObject.Find("New Game Object");
        if(toggledOff == false)
        {
            clickManager.panel.SetActive(false);
            clickManager.theLineDrawer.setObjectToPointAt(null, clickManager.panel.GetComponent<RectTransform>().position);
            foreach (Transform child in m_object.transform)
            {
                if(dataBase.getText(child.gameObject.name) != null)
                {
                    
                    child.gameObject.SetActive(false);
                }
            }
            toggledOff = true;
        }
        else
        {
            foreach (Transform child in m_object.transform)
            {
                child.gameObject.SetActive(true);
            }
            toggledOff = false;
        }
    }
    //handle message panel
    public void HidePanel()
    {
        messagePanel.SetActive(false);
    }
    public void DisplayError()
    {
        messagePanel.GetComponentInChildren<Text>().text = "To add a new object, please refresh your browser window.";
        messagePanel.SetActive(true);
    }
}
