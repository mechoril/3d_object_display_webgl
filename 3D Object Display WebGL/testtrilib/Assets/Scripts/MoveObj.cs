﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script handels the movement of the main Object and the camera in the scene.
public class MoveObj : MonoBehaviour
{
    [HideInInspector]
    public GameObject m_object = null;
    public GameObject loadedObject;
    [SerializeField]
    GameObject m_camera = null;
    [SerializeField]
    float zoom_speed = 2.5f;
    [HideInInspector]
    public bool canMove = true;

    public float translateSpeed = .5f;
    public float rotateSpeed = 50f;

    private Vector3 mouseOrigin;
    private Vector3 normalizedDirection;

    public bool isLoaded = false;
    // Update is called once per frame
    void Update()
    {
        loadedObject = GameObject.Find("Wrapper"); // this should be changed to something more efficient in the future
        if (isLoaded == false && loadedObject != null)
        {
            SetParent();
            isLoaded = true;
        }
        if (canMove == true)
        {
            //rotate object with arrow keys
            if (Input.GetKey("left"))
            {
                m_object.transform.Rotate(0, rotateSpeed * Time.deltaTime, 0, Space.World);
            }
            if (Input.GetKey("right"))
            {
                m_object.transform.Rotate(0, -rotateSpeed * Time.deltaTime, 0, Space.World);
            }
            if (Input.GetKey("up"))
            {
                m_object.transform.Rotate(-rotateSpeed * Time.deltaTime, 0, 0, Space.World);
            }
            if (Input.GetKey("down"))
            {
                m_object.transform.Rotate(rotateSpeed * Time.deltaTime, 0, 0, Space.World);
            }
            //zoom in and out with Q and E
            if (Input.GetKey(KeyCode.Q))
            {
                m_object.transform.position = m_object.transform.position + m_camera.transform.forward * zoom_speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.E))
            {
                m_object.transform.position = m_object.transform.position + m_camera.transform.forward * -zoom_speed * Time.deltaTime;
            }
            //move camera up,down,left, and right with WASD
            if (Input.GetKey(KeyCode.W))
            {
                m_camera.transform.Translate(0, translateSpeed * Time.deltaTime, 0);
            }
            if (Input.GetKey(KeyCode.A))
            {
                m_camera.transform.Translate(-translateSpeed * Time.deltaTime, 0, 0);
            }
            if (Input.GetKey(KeyCode.S))
            {
                m_camera.transform.Translate(0, -translateSpeed * Time.deltaTime, 0);
            }
            if (Input.GetKey(KeyCode.D))
            {
                m_camera.transform.Translate(translateSpeed * Time.deltaTime, 0, 0);
            }
            //rotate object with left mouse click and drag
            if (Input.GetMouseButton(0) && m_object != null)
            {
                float rotX = Input.GetAxis("Mouse X") * 100f * Mathf.Deg2Rad;
                float rotY = Input.GetAxis("Mouse Y") * 100f * Mathf.Deg2Rad;

                m_object.transform.Rotate(Vector3.up, -rotX, Space.World);
                m_object.transform.Rotate(Vector3.right, -rotY, Space.World);
            }
            if (Input.GetMouseButtonDown(1))
            {
                //right click was pressed    
                mouseOrigin = Input.mousePosition;
            }
            //move object with right mouse click and drag
            if (Input.GetMouseButton(1))
            {
                Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

                Vector3 move = new Vector3(pos.x * -10f * Time.deltaTime, pos.y * -10f * Time.deltaTime, 0);

                Camera.main.transform.Translate(move, Space.Self);
            }
            //zoom with scroll wheel
            if (m_object != null)
            {
                float scroll = Input.GetAxis("Mouse ScrollWheel");
                m_camera.transform.Translate(0, 0, scroll * 80f * Time.deltaTime, Space.Self);
            }
        }
    }
    //set up parent transform for movement purposes
    private void SetParent()
    {
        if (m_object != null)
        {
            Destroy(m_object);
        }
        m_object = new GameObject();
        m_object.transform.position = loadedObject.GetComponentInChildren<Renderer>().bounds.center;
        loadedObject.transform.SetParent(m_object.transform);
    }
}
