This is the documentation for the BYU Libary 3D Viewer. This documentation is for both the webgl and desktop versions.

Right now the 3D viewer is split into two different projects, a desktop version adn a webgl version. honestly, this is only due to time constaints. these both could pretty easily be put into one project
with a webgl scene and a desktop scene for the two builds. If i have time i will still try to do this. All it would require is some more file type checking in the trilib scripts and making sure zip files
are allowed in the trilib project settings.

Obviously not everything in this project is perfect, due to time constraints i couldn't do as much cleaning up as i would have liked. For Example, this application needs alot more error handling
such as when users try to import files other than .obj in the desktop version and zip files in the webgl version. Also alot of the "gameobject.find" statements that i meant to be temporary fixes
are still all over alot of my scripts. More or less, this projects needs some love to be as robust and bullet proof as possible. Below are my notes of most of what i did. The code is half what i have done and half
imported assets from the unity asset store the main one being trilib(for object importing), and i also used one called "Pointing Label" for the labels on the objects.

Important scripts
-LabeledObject - This script is responsible for getting the object ready for and performing saving and loading.
-MessageHandler - This script is used to temporarily print a message on the screen for the user to see.
-Database - The database script is stores all data on the labels of the loaded object.
-ClickManager - I added the click manager to handle the mouse click functionality as depending on the situation.
mouse clicks can mean different things such as clicking a text field or moving the object.
-SceneController - In general, this script manages what panels and fields are visible and when.
-MoveObj - This script handels the movement of the main Object and the camera in the scene.
-AddLabelOnClick - This script handles the functionality of adding a new label to the main object.
-LineDrawer - This script handles the line between a label and its info panel.
-ObjData - This script is used for saving and loading the main object.
-SaveSystem - This script deals with externally saving the object and its labels and loading them from an external file.

TriLib - TriLib is what i used for importing the Object into the scene, theres a lot of stuff that came with it from the asset store that might not be necessary. I just didn't have the time to trim the fat.
The main files i manipulated from the TriLib are AssetLoaderWindow, AssetLoader, and AssetLoaderBase.

-AssetLoader - This file is found in TriLib's main script folder. LoadFromFileWithTextures is the only function i had to mess with. I added a few lines at the beginning of the function to save the path for later.
-AssetLoaderBase - This file is found in TriLib's main script folder. I added some code to the BuildGameObject function to make it possible to add loaded labels and new labels.
If you check the zip loading function SetupZipLoading, i added something here for the webgl version(currently unused in the desktop version)
-AssetLoaderWindow - This file is found in TriLib's samples script folder. I really only removed unecessary stuff from this script that dealt with the first example scene trilib came with.

Prefabs
The Bubble prefab is placed on an object for the label system.
The SpherePointer object is also for the label system, it is used as an endpoint for the linedrawing system.
The panel prefab is also for the label system and is the panel the label ionformation appears on.

Materials
The bubble material is for the bubble prefab used in the label system.

UI
Most of the UI i got from an asset from the asset store called "139 vector icons", I also downloaded a few from free icon websites on the internet.

Other Info
When an object is saved, it stores its data in the same folder it is in. The saved data is a file called "Object.savedata".

Usefull additions to what is already done

combine web and desktop into one project
add functionality so use can manipulate zoom, rotate, and translate speeds
add functionality so user can manipluate background color(done), label color, and bubble color(done) 3 and set up a a save system to save these preferences 